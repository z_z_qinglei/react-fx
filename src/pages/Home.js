import React, { Component } from 'react';
import { Tabs, Spin } from 'antd';
import { TableCurrencyRisk, SelectCurrency, ChartCurrencyRisk } from '../components';

/** Mock Data */
import { genData, buckets } from '../utils/data'

const { TabPane } = Tabs;

export default class HomePage extends Component {
    constructor(props) {
        super(props)

        this.state = {
            isReady: false,
            buckets,
            data: [],
            currency: 'INR'
        }
    }

    componentDidMount() {
        this.setState({
            data: genData(this.state.buckets),
            isReady: true
        })
        this.interval = setInterval(() => {
            this.setState({
                data: genData(this.state.buckets)
            })
        }, 5000);
    }

    componentWillUnmount() {
        if (!!this.interval) {
            clearInterval(this.interval)
        }
    }

    onCurrencySelected = (currency) => {
        this.setState({
            currency: currency,
            data: genData(this.state.buckets)
        })
    }

    onRiskChange = (row) => {
        const newData = [...this.state.data];
        const newBuckets = [...this.state.buckets]
        const index = newData.findIndex(item => row.key === item.key);
        const item = newData[index];
        newBuckets.splice(index, 1, {
            ...newBuckets[index], risk: row.risk
        })
        newData.splice(index, 1, {
            ...item,
            ...row,
        });
        this.setState({
            buckets: newBuckets,
            data: newData,
        });
    };

    render() {
        const { currency, data, isReady } = this.state
        return (
            !isReady ?
                <Spin /> :
                <div>
                    <SelectCurrency value={currency} onChange={this.onCurrencySelected} />

                    <Tabs defaultActiveKey="1">
                        <TabPane tab="Bucket Statistics" key="1">
                            <TableCurrencyRisk data={data} currency={currency} onRiskChange={this.onRiskChange} />
                        </TabPane>
                        <TabPane tab="Chart" key="2">
                            <ChartCurrencyRisk data={data} />
                        </TabPane>
                    </Tabs>
                </div>
        )
    }
}

