import React from 'react';
import { Layout } from 'antd';
const { Footer } = Layout;

const CustomFooter = () => {
	return (
		<Footer>
			<span>Foreign Currency Risk © 2019</span>
		</Footer>
	)
}

export default CustomFooter;
