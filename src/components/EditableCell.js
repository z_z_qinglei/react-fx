import React, { Component } from 'react';
import { Input, Form } from 'antd';

const EditableContext = React.createContext();
const EditableRow = ({ form, index, ...props }) => (
    <EditableContext.Provider value={form}>
        <tr {...props} />
    </EditableContext.Provider>
);

export default class EditableCell extends Component {
    state = {
        editing: false,
    };

    toggleEdit = () => {
        const editing = !this.state.editing;
        this.setState({ editing }, () => {
            if (editing) {
                this.input.focus();
            }
        });
    };

    save = e => {
        const { record, handleSave } = this.props;
        this.form.validateFields((error, values) => {
            if (error && error[e.currentTarget.id]) {
                return;
            }
            this.toggleEdit();
            handleSave({ ...record, ...values });
        });
    };

    validateNumber = (val) => {
        if (isNaN(val)) {
            return {
                validateStatus: 'error',
                errorMsg: 'Risk should be a number',
            };

        }
        return {
            validateStatus: 'success',
            errorMsg: null,
        };
    }

    validate = e => {
        this.setState({

        })
    }

    renderCell = form => {
        this.form = form;
        const { children, dataIndex, record, title } = this.props;
        const { editing } = this.state;
        return editing ? (
            <Form.Item style={{ margin: 0 }}>
                {form.getFieldDecorator(dataIndex, {
                    getValueFromEvent: (e) => {
                        const convertedValue = Number(e.currentTarget.value);
                        if (isNaN(convertedValue)) {
                            return Number(this.form.getFieldValue(dataIndex));
                        } else {
                            return convertedValue;
                        }
                    },
                    rules: [
                        {
                            required: true,
                            message: `${title} is required.`,
                        }
                    ],
                    initialValue: record[dataIndex],
                })(<Input ref={node => (this.input = node)} onPressEnter={this.save} onChange={this.validate} onBlur={this.save} />)}
            </Form.Item>
        ) : (
                <div
                    className="editable-cell-value-wrap"
                    style={{ paddingRight: 24 }}
                    onClick={this.toggleEdit}
                >
                    {children}
                </div>
            );
    };

    render() {
        const {
            editable,
            dataIndex,
            title,
            record,
            index,
            handleSave,
            children,
            ...restProps
        } = this.props;
        return (
            <td {...restProps}>
                {editable ? (
                    <EditableContext.Consumer>{this.renderCell}</EditableContext.Consumer>
                ) : (
                        children
                    )}
            </td>
        );
    }
}

export const EditableFormRow = Form.create()(EditableRow);
