import React from 'react';
import { NavLink, withRouter } from 'react-router-dom';
import { Layout } from 'antd';
const { Header } = Layout;

const TopBar = () => {

    return (
        <Header className="header-nav-wrapper" >
            <div className="nav-logo-wrapper">
                <NavLink to="/">
                    <div className="vertical-center">
                        <span>The Foreign Currency</span>
                    </div>
                </NavLink>
            </div>
        </Header >
    )
}

export default withRouter(TopBar);
