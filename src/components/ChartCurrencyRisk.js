import React from 'react';
import { Row } from 'antd';
import { DynamicChart } from './charts'

const getChartData = (key, data, multiplier) => {
    if (!data || !Array.isArray(data) || data.length < 1) {
        return []
    }
    return data.map(item => {
        return {
            label: item.bucket,
            y: !multiplier ? item[key] : item[key] * multiplier
        }
    })
}

const keys_constant = ['risk', 'move']
const keys_rate = ['close_rate', 'live_rate']
const keys_dynamic = ['nb_risk', 'live_risk', 'nb_pl', 'live_pl', 'pl']
const get_title = (key) => {
    return key.replace('_', ' ').toUpperCase()
}
const ChartCurrencyRisk = ({ data }) => {
    return (
        <div>
            {/* <DynamicColumnChart type='line' dataPoints={getChartData('random', data)} /> */}
            <Row gutter={24}>
                {keys_constant.map(key => {
                    return <DynamicChart key={key} dataPoints={getChartData(key, data)} title={get_title(key)} />
                })}
            </Row>
            <Row gutter={24}>
                {keys_rate.map(key => {
                    return <DynamicChart key={key} dataPoints={getChartData(key, data, 100)} title={get_title(key)} suffix='%' yValueFormatString="#0.##'%'" />
                })}
            </Row>
            <Row gutter={16}>
                {keys_dynamic.map(key => {
                    return <DynamicChart key={key} dataPoints={getChartData(key, data)} title={get_title(key)} />
                })}
            </Row>
        </div>
    );
}

export default ChartCurrencyRisk;