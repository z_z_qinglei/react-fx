import React from 'react';
import { Select } from 'antd';
const { Option } = Select;

// For demo only
const currency_symbols = {
    'USD': '$', // US Dollar
    'EUR': '€', // Euro
    'GBP': '£', // British Pound Sterling
    'JPY': '¥', // Japanese Yen
    'INR': '₹', // Indian Rupee
    'CNH': '¥', // Chinese Yuan Overseas
    'KRW': '₩', // South Korean Won
    'PHP': '₱', // Philippine Peso
    'THB': '฿', // Thai Baht
    'VND': '₫', // Vietnamese Dong
};

const SelectCurrency = ({ value, onChange }) => {
    return (
        <div className="currency-dropdown"><span>Currency：</span>
            <Select
                value={value}
                onChange={onChange}
                style={{ width: 120 }}
            >
                {Object.keys(currency_symbols).map(key => {
                    return <Option value={key} key={key}>{key} ({currency_symbols[key]})</Option>
                })}
            </Select>
        </div>
    )
}

export default SelectCurrency;
