import React from 'react';
import { Table } from 'antd';
import { default as Statistic } from './StatisticDisplay';
import { default as EditableCell, EditableFormRow } from './EditableCell';
// column format
const columns = [
    {
        title: 'Bucket',
        dataIndex: 'bucket',
        key: 'bucket',
        fixed: 'left',
        width: 80,
    }, {
        title: 'Random',
        dataIndex: 'random',
        key: 'random',
        render: (num) => {
            return <Statistic value={Math.round(num)} />
        }
    }, {
        title: 'Risk',
        dataIndex: 'risk',
        key: 'risk',
        editable: true,
        render: (num) => {
            return <Statistic value={Math.round(num)} />
        }
    }, {
        title: 'NB Risk',
        dataIndex: 'nb_risk',
        key: 'nb_risk',
        render: (num) => {
            return <Statistic value={Math.round(num)} />
        }
    }, {
        title: 'Live Risk',
        key: 'live_risk',
        dataIndex: 'live_risk',
        render: (num) => {
            return <Statistic value={Math.round(num)} />
        }
    }, {
        title: 'Close (%)',
        key: 'close_rate',
        dataIndex: 'close_rate',
        render: (num) => {
            return <Statistic value={num * 100} precision={2} suffix={'%'} />
        }
    }, {
        title: 'Live (%)',
        key: 'live_rate',
        dataIndex: 'live_rate',
        render: (num) => {
            return <Statistic value={num * 100} precision={2} suffix={'%'} />
        }
    }, {
        title: 'Move',
        key: 'move',
        dataIndex: 'move',
        render: (num) => {
            return <Statistic value={Math.round(num)} />
        }
    }, {
        title: 'Live PL',
        key: 'live_pl',
        dataIndex: 'live_pl',
        render: (num) => {
            return <Statistic value={Math.round(num)} />
        }
    }, {
        title: 'NB PL',
        key: 'nb_pl',
        dataIndex: 'nb_pl',
        render: (num) => {
            return <Statistic value={Math.round(num)} />
        }
    }, {
        title: 'PL',
        key: 'pl',
        dataIndex: 'pl',
        render: (num) => {
            return <Statistic value={Math.round(num)} />
        }
    },
];


function dataWithSum(data) {
    if (!data || !Array.isArray(data) || data.length < 1) {
        return data
    }

    let sumData = data.reduce((acc, n) => {
        for (var prop in n) {
            if (acc.hasOwnProperty(prop)) acc[prop] += n[prop]
            else acc[prop] = n[prop];
        }
        return acc
    }, {})

    // process some data with averaging
    for (var key in sumData) {
        switch (key) {
            case 'bucket':
                sumData[key] = 'Total'
                break
            case 'close_rate':
            case 'live_rate':
            case 'move':
                sumData[key] /= data.length;
                break
            case 'random':
                sumData[key] = undefined
                break
            default:
                break
        }
    }
    return [...data, sumData]
}

const TableCurrencyRisk = ({ data, currency, onRiskChange }) => {
    const header = `Currency: ${!!currency && (typeof currency === 'string') ? currency.toUpperCase() : ''}`
    const final_data = dataWithSum(data)
    const components = {
        body: {
            row: EditableFormRow,
            cell: EditableCell,
        },
    };
    const final_columns = columns.map(col => {
        if (!col.editable) {
            return col;
        }
        return {
            ...col,
            onCell: record => ({
                record,
                editable: col.editable && (record.bucket !== 'Total'),
                dataIndex: col.dataIndex,
                title: col.title,
                handleSave: onRiskChange,
            }),
        };
    });
    return (
        <div>
            <Table
                components={components}
                rowClassName={() => 'editable-row'}
                columns={final_columns} dataSource={final_data}
                rowKey={row => row.bucket} pagination={false}
                title={() => header}
                scroll={{ x: '100%-6rem' }}
            />
        </div>
    )
}

export default TableCurrencyRisk
