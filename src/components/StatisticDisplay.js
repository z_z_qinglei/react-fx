import React from 'react';
import { Statistic } from 'antd';

const negativeColor = '#f5222d' // for negative

const StatisticDisplay = ({ value, precision, prefix, suffix }) => {
    if (isNaN(value)) {
        return '--'
    }
    return <Statistic value={value} prefix={prefix} suffix={suffix} precision={precision ? precision : 0}
        valueStyle={{
            color: (value >= 0 ? 'inherit' : negativeColor)
        }} />
}

export default StatisticDisplay;
