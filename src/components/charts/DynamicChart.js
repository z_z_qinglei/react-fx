import React from 'react';
import { Col } from 'antd';
import CanvasJSReact from '../../assets/canvasjs.react';
var CanvasJSChart = CanvasJSReact.CanvasJSChart;

const DynamicChart = ({ title, dataPoints, chartType, yValueFormatString, span, suffix }) => {

	const options = {
		theme: "light2",
		subtitles: [{
			text: title
		}],
		axisY: {
			title,
			suffix: suffix ? suffix : ''
		},
		data: [{
			type: chartType ? chartType : 'column',
			yValueFormatString,
			indexLabel: "{y}",
			dataPoints
		}]
	}
	span = span ? span : {
		xs: 24, sm: 24, md: 12, xl: 8
	}

	return (
		<Col {...span}>
			<h3>{title}</h3>
			<CanvasJSChart options={options}
			/>
		</Col>
	);
}


export default DynamicChart;