import faker from 'faker';

export function getRandom() {
    return faker.random.number({ min: -100, max: 100 })
}