import { getRandom } from './index'

// pre-defined hardcoded data
export const buckets = [
    { bucket: '1D', risk: 1000, close_rate: 0.05 }, // key and hardcoded risk
    { bucket: '1W', risk: 1200, close_rate: 0.053 },
    { bucket: '1M', risk: 1400, close_rate: 0.056 },
    { bucket: '3M', risk: 1600, close_rate: 0.059 },
    { bucket: '6M', risk: 1800, close_rate: 0.062 },
    { bucket: '1Y', risk: -1600, close_rate: 0.065 },
    { bucket: '2Y', risk: -1400, close_rate: 0.068 },
    { bucket: '5Y', risk: 1600, close_rate: 0.071 },
    { bucket: '10Y', risk: 1800, close_rate: 0.074 },
    { bucket: '20Y', risk: 2000, close_rate: 0.077 }
];

const dataItem = (item) => {
    return {
        ...item,
        key: item.bucket,
        random: getRandom() // get random 
    }
}

export const genData = (originData) => {

    return originData // original buckets
        .map(item => dataItem(item)) // add random
        .map(el => {
            return {
                ...el, nb_risk: (el.risk * 5 * el.random) / 1000,  // calc nb_risk
                live_rate: el.close_rate + el.random / 1000, // calc live rate
                nb_pl: el.random * 25, // calc nb pl
            }
        })
        .map(el => {
            return {
                ...el, live_risk: el.risk + el.nb_risk,  // calc live_risk
                move: (el.live_rate - el.close_rate) * 10000 // calc move
            }
        })
        .map(el => { return { ...el, live_pl: el.risk * el.move, } }) // calc live pl
        .map(el => { return { ...el, pl: el.live_pl + el.nb_pl } }) // calc pl
}
