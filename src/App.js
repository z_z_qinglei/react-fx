import React, { Component } from 'react';
import { HashRouter, Switch, Route, Redirect } from 'react-router-dom'
import { Layout } from 'antd';
import { TopBar, Footer } from './components'
import {
  HomePage
} from './pages';

// style
import 'antd/dist/antd.css';
import './App.scss';

const { Content } = Layout;

class App extends Component {
  render() {
    return (
      <HashRouter>
        <Layout className="layout">
          <TopBar />
          <Content className='content-wrapper'>
            <Switch>
              <Route exact path='/' render={() => <Redirect to="/home" />} />
              <Route exact path='/home' component={HomePage} />
              <Redirect to="/home" />
            </Switch>
          </Content>
          <Footer />
        </Layout>
      </HashRouter>
    );
  }
}

export default App;
